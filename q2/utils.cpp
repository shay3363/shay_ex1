#include "utils.h"
#include "stack.h"
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	initStack(s);
	int i = 0;
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
	cleanStack(s);
}
int* reverse10()
{
	stack* s = new stack;
	initStack(s);
	int i = 0;
	int input = 0;
	int* arr = new int[10];
	for (i = 0; i < 10; i++)
	{
		std::cin >> input;
		push(s, input);
	}
	for (i = 0; i < 10; i++)
	{
		arr[i] = pop(s);
	}
	cleanStack(s);
	return arr;
}