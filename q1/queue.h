#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* _numbers;
	unsigned int _numbersLenght;
	int _lastPlace;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
void printQueue(queue* q);
void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty
bool isFull(queue* q);
bool isEmpty(queue* q);
#endif /* QUEUE_H */