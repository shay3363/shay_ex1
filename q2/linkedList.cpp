#include "linkedList.h"
node * crateNode(unsigned int data, node* next)
{
	node* newNode = new node;
	newNode->_data = data;
	newNode->_next = next;
	return newNode;
}
void printList(node* head)
{
	if (head)
	{
		std::cout << head->_data << "\n";
		printList(head->_next);
	}
}
void addNodeTop(node** headPtr, node* newnode)
{
	newnode->_next = *headPtr;
	*headPtr = newnode;
}
int removeNodeTop(node** headPtr)
{
	node* head = *headPtr;
	int data = -1;
	if (head)
	{
		data = head->_data;
		*headPtr = head->_next;
		delete head;
	}
	return data;
}
void cleanList(node* head)
{
	node* next = 0;
	if (head)
	{
		next = head->_next;
		delete head;
		cleanList(next);
	}
}