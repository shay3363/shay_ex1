#include "queue.h"
void initQueue(queue* q, unsigned int size)
{
	q->_numbersLenght = size;
	q->_numbers = new unsigned int[q->_numbersLenght];
	q->_lastPlace = -1;
}
void cleanQueue(queue* q)
{
	delete[] q->_numbers;
}
bool isEmpty(queue* q)
{
	bool flag = false;
	if (q->_lastPlace == -1)
	{
		flag =  true;
	}
	return flag;
}
bool isFull(queue* q)
{
	bool flag = false;
	if (q->_lastPlace == q->_numbersLenght - 1)
	{
		flag = true;
	}
	return flag;
}
void printQueue(queue* q)
{
	int i = 0;
	if (isEmpty(q))
	{
		std::cout << "queue is empty\n";
	}
	else
	{
		for (i = 0; i <= q->_lastPlace; i++)
		{
			std::cout << q->_numbers[i] << "\n";
		}
	}
}
void enqueue(queue* q, unsigned int newValue)
{
	if (!isFull(q))
	{
		q->_lastPlace++;
		q->_numbers[q->_lastPlace] = newValue;
	}
}
int dequeue(queue* q)
{
	int i = 0;
	int first = q->_numbers[0];
	if (!isEmpty(q))
	{
		q->_lastPlace--;
		for (i = 0; i <= q->_lastPlace; i++)
		{
			q->_numbers[i] = q->_numbers[i + 1];
		}
	}
	else
	{
		first = -1;
	}
	return first;
}