#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iostream>
typedef struct node
{
	unsigned int _data; 
	struct node* _next;
}node;
node * crateNode(unsigned int data, node* next);
void printList(node* head);
void addNodeTop(node** headPtr, node* newNode);
int removeNodeTop(node** headPtr);
void cleanList(node* head);
#endif /* LINKEDLIST_H */