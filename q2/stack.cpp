#include "stack.h"
void push(stack* s, unsigned int element)
{
	addNodeTop(&(s->head), crateNode(element, s->head));
}
int pop(stack* s)
{
	return removeNodeTop(&(s->head));
}
void printStack(stack* s)
{
	printList(s->head);
}
void initStack(stack* s)
{
	s->head = 0;
}
void cleanStack(stack* s)
{
	cleanList(s->head);
}